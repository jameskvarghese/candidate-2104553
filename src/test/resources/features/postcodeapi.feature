Feature: Query a postcode and receive a 200 response

 
  Scenario Outline: Query a postcode and receive a 200 response
    Given I send a query to post code api "<url>"
    Then I will get a successful response code
    Examples:
      |url|
      |https://api.postcodes.io/postcodes/SW1P4JA|

