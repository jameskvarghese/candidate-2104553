package com.pages;
import static org.junit.Assert.assertEquals;
import java.util.List;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.gov.uk/check-uk-visa/y")
public class HomePageRS extends PageObject {

    @FindBy(xpath = "/html/body/div[6]/main/div/div/form/div/div/div/select")
    WebElement dropDown;

    @FindBy(xpath = "//select[@id='response']")
    WebElement selectCountry;

    @FindBy(xpath = "//*[@id=\"current-question\"]/button")
    WebElement countryButton;

    @FindBy(name = "response")
    WebElement selectStudy;

    @FindBy(xpath = "/html/body/div[6]/main/div/div/form/div/button")
    WebElement tourismButton;

    @FindBy(xpath = "//input[@id='response-1']")
    WebElement familyVisa;

    @FindBy(xpath = "//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")
    WebElement familyButton;

    @FindBy(xpath = "//h2[contains(@class,'govuk-!-margin-bottom-6')]")
    WebElement validateTitle;


 
    public void clickdropdown() throws InterruptedException {
    	dropDown.click();
 
    }
    public void selectcountry(String countryname) throws InterruptedException {
        Select cntrysel = new Select(selectCountry);
        cntrysel.selectByIndex(161);

    }
    public void clicknextpage() throws InterruptedException {

        countryButton.click();
    }
    public void selecttourism() throws InterruptedException {
        ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(200,600)", "");
        List<WebElement> radiobuttons = getDriver().findElements(By.name("response"));
        for(WebElement radiobutton: radiobuttons) {
        if(radiobutton.getAttribute("value").equals("tourism"))
        radiobutton.click();
        }

    }
    public void clicktourismbutton() throws InterruptedException {
        tourismButton.click();
    }
    public void selectfamilyno() throws InterruptedException {
        ((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(200,300)", "");
        List<WebElement> radiobuttons = getDriver().findElements(By.name("response"));
        for(WebElement radiobutton: radiobuttons) {
        if(radiobutton.getAttribute("value").equals("no"))
        radiobutton.click();  }

    }
    public void selectfamily() throws InterruptedException {
        familyButton.click();
    }
    public void visavalidation() throws InterruptedException {
        String titlevalid_act ="You’ll need a visa to come to the UK";
        String titlevalid_exp = validateTitle.getText();
        assertEquals(titlevalid_exp, titlevalid_act);System.out.println("Actual and Expected Success response is matching ");
    }

}