package com.pages;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.thucydides.core.pages.PageObject;

public class PostCodePage extends PageObject {
	
    public Response response;
    int ActResponseCode = 200;
    
    public void requestPostCode(String url) {
    	
	RestAssured.baseURI = url;
    RestAssured.basePath = "";
    response = given().contentType(ContentType.JSON).log().all().get(url);
    System.out.println("Response :" + response.asString());
    
    }
    
    public void responsePostCode() {
    	
    	int ExpResponseCode = response.getStatusCode();
        assertEquals(ActResponseCode, ExpResponseCode);
        System.out.println("Expected Response Code :" + ExpResponseCode);
    }
}
