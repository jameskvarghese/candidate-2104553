package com.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions
(features = "./src/test/resources/features",
glue = { "com.stepDF" },
plugin = { "html:target/cucumber-html-report", "json:target/cucumber.json" }
)
public class CucumberFullPackRunner {

}
