package com.stepDF;
import com.pages.PostCodePage;
import cucumber.api.java.en.Given;
import io.restassured.response.Response;

public class PostCodeSearch {
	
	PostCodePage postCodePage;
    public Response response;
    int ActResponseCode = 200;
    @Given("^I send a query to post code api \"([^\"]*)\"$")
    public void I_send_a_query_to_post_code_api(String url)
    {    	
    	postCodePage.requestPostCode(url);
    }

    @Given("^I will get a successful response code$")
    public void I_will_get_a_successful_response_code()
    {
    	postCodePage.responsePostCode();
    }
}
